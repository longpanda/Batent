# Batent

#### 项目介绍
虽然Windows平台中的Bat批处理不管是从技术上还是从使用上来说都已经非常过时了，但是在一些简单的应用场景中还是有其用武之地的。
另外，由于生产环境或者项目历史的原因，我们有时候也可能会维护、新增一些批处理脚本。

默认情况下批处理的功能比较弱。另外，有些功能虽然可以利用现有的命令组合实现，但使用起来比较麻烦。

举个例子：获取一个字符串的长度，这个简单的需求在批处理里面是没有标准命令的，虽然有些变通的实现方法，但是非常麻烦。

Batent是一个独立的exe可执行文件，集成了一些常用的命令，和批处理配套使用，方便脚本的编写。从某些方面Batent有点类似于Linux下的BusyBox（当然功能没有那么强大）。


#### 软件架构
Batent就是简单地利用C/C++以及Windows API实现了一些简单、常用的命令，并统一集成在一个exe可执行文件中。


#### 安装教程
源码在Windows平台下编译之后生成Batent.exe。</br>
支持MinGW和MSVC编译器，对应两个编译脚本 BuildMinGW.bat 和 BuildMSVC.bat。先在脚本开始的地方修改编译器的路径，然后执行即可。

#### 使用说明
直接在Bat批处理中调用Batent.exe即可。
Batent.exe是一个独立的可执行文件，不依赖其他DLL

#### 命令手册
本站Wiki [链接](https://gitee.com/longpanda/Batent/wikis/pages)

#### 执行结果
Batent通过两种方式返回命令执行的结果：
>##### 1. 通过返回值
>>对于一些整数类型的结果，通过返回值返回，比如字符串长度、文件大小等。<br/>
>>批处理里面调用完Batent命令之后直接使用 %ERRORLEVEL% 这个环境变量即可获得结果。
>>```
>>Batent.exe fs.get_file_size C:\test.bin
>>set FileSize=%ERRORLEVEL%
>>...
>>```

>##### 2. 通过打印（标准输出）
>>对于非整数类型的结果，或者超过了32位有符号整数范围的结果，Batenet通过printf打印输出。<br/>
>>对于结果中包含多个字段的，中间会以空格隔开。<br/>
>>批处理里面调用完Batent命令之后，可以使用 For /F 语句获得命令结果，比如：<br/>
>>```
>>For /f %%i in ('Batent.exe fs.get_file_size_ex C:\test.bin') do (
>>      set LargeFileSize=%%i
>>)
>>```
 
#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
