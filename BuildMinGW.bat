@echo off
setlocal EnableDelayedExpansion

::*********************************************
::Set your MinGW path here
::*********************************************
set MinGWPath=C:\MinGW\bin





set CXXFLAG=-Wall -O2
set PATH=%PATH%;%MinGWPath%

if exist obj\nul  rd /q /s obj>nul 2>&1
mkdir obj

For /F %%i in ('dir /b src\*.cpp') do (
    
    echo Build %%i ...
    g++ %CXXFLAG% src\%%i -c -o obj\%%~ni.o
    
    if !ERRORLEVEL! NEQ 0 (
        goto Failed
    )
)

g++ %CXXFLAG% obj\*.o -o obj\Batent.exe
if %ERRORLEVEL% NEQ 0 (
    goto Failed
)

echo.
echo *******************
echo Build Success
echo *******************
echo.

goto End

:Failed
echo.
echo *******************
echo Build Failed
echo *******************
echo.

:End
pause


