@echo off
setlocal EnableDelayedExpansion

::*********************************************
::Set your VC path here
::*********************************************
set VCDir=C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC
set WinSDKDir=C:\Program Files (x86)\Windows Kits\8.1
set WinSDKLibDir=C:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x86






set PATH=%PATH%;%VCDir%\bin

set CXXFLAG=/GS /GL /analyze- /W3 /Gy /Zc:wchar_t /Zi /Gm- /O2 /fp:precise /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_LIB" /D "_MBCS" /errorReport:prompt /WX- /Zc:forScope /Gd /Oy- /Oi /MT /EHsc /nologo  /I"%VCDir%\include" /I"%VCDir%\atlmfc\include" /I"%WinSDKDir%\Include\um" /I"%WinSDKDir%\Include\shared" /I"%WinSDKDir%\Include\winrt"

set LDFLAG=/OUT:"obj\Batent.exe" /MANIFEST /LTCG /NXCOMPAT /DYNAMICBASE "kernel32.lib" "user32.lib" "gdi32.lib" "winspool.lib" "comdlg32.lib" "advapi32.lib" "shell32.lib" "ole32.lib" "oleaut32.lib" "uuid.lib" "odbc32.lib" "odbccp32.lib" /DEBUG /MACHINE:X86 /OPT:REF /SAFESEH /INCREMENTAL:NO  /SUBSYSTEM:CONSOLE /MANIFESTUAC:"level='asInvoker' uiAccess='false'"  /OPT:ICF /ERRORREPORT:PROMPT /NOLOGO /TLBID:1 /LIBPATH:"%VCDir%\lib" /LIBPATH:"%VCDir%\atlmfc\lib" /LIBPATH:"%WinSDKLibDir%"

if exist obj\nul  rd /q /s obj>nul 2>&1
mkdir obj

For /F %%i in ('dir /b src\*.cpp') do (
    
    cl.exe %CXXFLAG% src\%%i /c
    if !ERRORLEVEL! NEQ 0 (
        goto Failed
    )
    
    move %%~ni.obj  obj\%%~ni.obj>nul
)

link.exe %LDFLAG% obj\*.obj
if %ERRORLEVEL% NEQ 0 (
    goto Failed
)

del /f /q *.pdb
echo.
echo *******************
echo Build Success
echo *******************
echo.

goto End

:Failed
echo.
echo *******************
echo Build Failed
echo *******************
echo.

:End
pause

