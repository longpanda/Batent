#ifndef __DATE_CMD_H__
#define __DATE_CMD_H__

#define CHECK_UTC_PARA(index, args) \
{\
    if (args.size() == (index + 1) && args[index] != "/u") \
    {\
        return ERR_INVALID_PARA;\
    }\
}

#define DATE_STR_REPLACE(key, fmt, ...) \
{\
    sprintf_s(szBuf, sizeof(szBuf) - 1, fmt, ##__VA_ARGS__); \
    while ((Pos = DateStr.find(key)) != string::npos) \
    {\
        DateStr = DateStr.replace(Pos, strlen(key), szBuf);\
    }\
}


#endif