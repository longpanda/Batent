#ifndef __FS_CMD_H__
#define __FS_CMD_H__

namespace Batent {
namespace FS 
{
    bool is_directory(const string &szFileName);
    bool is_file_exist(const string &szFileName);
    long long get_file_size(const string &szFileName);
}}

#endif
