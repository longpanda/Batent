#include "Batent.h"
using namespace Batent;

bool Util::err_enable = true;

void Util::setErrLogEnable(bool enable)
{
    Util::err_enable = enable;
}

void Util::outPut(const char *fmt, ...)
{
    va_list arg;
    char szBuf[1024];

    va_start(arg, fmt);

#if (defined __MINGW32__) || (defined __MINGW64__)
    vsnprintf(szBuf, sizeof(szBuf), fmt, arg);
#else
    vsprintf_s(szBuf, sizeof(szBuf), fmt, arg);
#endif
    va_end(arg);

    printf("%s", szBuf);
}

void Util::errlog(const char *fmt, ...)
{
    va_list arg;
    char szBuf[512];

    if (!Util::err_enable)
    {
        return;
    }

    va_start(arg, fmt);
#if (defined __MINGW32__) || (defined __MINGW64__)
    vsnprintf(szBuf, sizeof(szBuf), fmt, arg);
#else
    vsprintf_s(szBuf, sizeof(szBuf), fmt, arg);
#endif
    va_end(arg);

#if (defined __MINGW32__) || (defined __MINGW64__)
    fprintf(stderr, "%s", szBuf);
#else
    fprintf_s(stderr, "%s", szBuf);
#endif
}

