#include "Batent.h"
using namespace Batent;

namespace Batent {
namespace MISC
{
    DECLARE_CMD_CLASS(CMD1, 1, 1, "misc.sleep",     "Sleep for a few seconds");
}}



//misc.sleep
int MISC::CMD1::run(const vector<string> &args)
{
    unsigned long long Secs;

    CHECK_INT_PARA(Secs, 0, 10, 4000000);
    Sleep((DWORD)(Secs * 1000));

    return 0;
}

