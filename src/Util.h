#ifndef __BATENT_UTIL_H__
#define __BATENT_UTIL_H__

namespace Batent
{
    class Util
    {
    private:
        static bool err_enable;
        Util();

    public:
        static void setErrLogEnable(bool enable);
        static void outPut(const char *fmt, ...);
        static void errlog(const char *fmt, ...);

        

    };


    #define error Util::errlog
    #define print Util::outPut

#if (defined __MINGW32__) || (defined __MINGW64__)
#define sprintf_s snprintf
#endif

#define CHECK_INT_PARA(ullVal, index, radix, max) \
{\
    ullVal = strtoull(args[index].c_str(), NULL, rate); \
    if (ullVal > max)\
    {\
        return ERR_INVALID_PARA;\
    }\
}


#define ERR_SUCCESS          0
#define ERR_INVALID_PARA    -1
#define ERR_FILE_NOT_EXIST  -2
#define ERR_FILE_OPEN_FAIL  -3
#define ERR_OUT_OF_RANGE    -4
#define ERR_NOT_SUPPORTED   -5


}
#endif
