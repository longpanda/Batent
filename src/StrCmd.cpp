#include "Batent.h"
using namespace Batent;

namespace Batent {
namespace STR
{
    DECLARE_CMD_CLASS(CMD1, 1, 1, "str.get_length",     "Get string length");
    DECLARE_CMD_CLASS(CMD2, 2, 2, "str.index_of",       "Get first index of str2 in str1");
    DECLARE_CMD_CLASS(CMD3, 1, 1, "str.tolower",        "Convert char to lower case");
    DECLARE_CMD_CLASS(CMD4, 1, 1, "str.toupper",        "Convert char to upper case");
    DECLARE_CMD_CLASS(CMD5, 1, 1, "str.is_all_digit",   "Check if all char is digit");
    DECLARE_CMD_CLASS(CMD6, 1, 1, "str.is_all_xdigit",  "Check if all char is xdigit");  
}}


//str.get_length
int STR::CMD1::run(const vector<string> &args)
{
    return (int)args[0].size();
}

//str.index_of
int STR::CMD2::run(const vector<string> &args)
{
    string::size_type Index = args[0].find(args[1]);
    if (Index == string::npos)
    {
        return -2;
    }
    else
    {
        return (int)Index;
    }
}

//str.tolower
int STR::CMD3::run(const vector<string> &args)
{
    for (size_t t = 0; t < args[0].size(); t++)
    {
        print("%c", tolower(args[0][t]));
    }
    print("\n");

    return 0;
}

//str.toupper
int STR::CMD4::run(const vector<string> &args)
{
    for (size_t t = 0; t < args[0].size(); t++)
    {
        print("%c", toupper(args[0][t]));
    }
    print("\n");
    
    return 0;
}


//str.is_all_digit
int STR::CMD5::run(const vector<string> &args)
{
    for (size_t t = 0; t < args[0].size(); t++)
    {
        if (!isdigit(args[0][t]))
        {
            return -2;
        }
    }

    return 0;
}

//str.is_all_xdigit
int STR::CMD6::run(const vector<string> &args)
{
    for (size_t t = 0; t < args[0].size(); t++)
    {
        if (!isxdigit(args[0][t]))
        {
            return -2;
        }
    }

    return 0;
}

