#include "Batent.h"
using namespace Batent;

namespace Batent {
namespace DATE
{
    DECLARE_CMD_CLASS(CMD1, 0, 1, "date.year",       "Get current year");
    DECLARE_CMD_CLASS(CMD2, 0, 1, "date.month",      "Get current month");
    DECLARE_CMD_CLASS(CMD3, 0, 1, "date.day",        "Get current day");
    DECLARE_CMD_CLASS(CMD4, 0, 0, "date.1970s",      "Get total seconds from 1970/1/1 00:00:00");
    DECLARE_CMD_CLASS(CMD5, 1, 2, "date.format",     "Print format date string");
}}


//date.year
int DATE::CMD1::run(const vector<string> &args)
{
    SYSTEMTIME SysTime;

    CHECK_UTC_PARA(0, args);

    if (args.size() == 0)
    {
        GetLocalTime(&SysTime);
    }
    else
    {
        GetSystemTime(&SysTime);
    }

    return (int)SysTime.wYear;
}

//date.month
int DATE::CMD2::run(const vector<string> &args)
{
    SYSTEMTIME SysTime;

    CHECK_UTC_PARA(0, args);

    if (args.size() == 0)
    {
        GetLocalTime(&SysTime);
    }
    else
    {
        GetSystemTime(&SysTime);
    }

    return (int)SysTime.wMonth;
}


//date.day
int DATE::CMD3::run(const vector<string> &args)
{
    SYSTEMTIME SysTime;

    CHECK_UTC_PARA(0, args);

    if (args.size() == 0)
    {
        GetLocalTime(&SysTime);
    }
    else
    {
        GetSystemTime(&SysTime);
    }

    return (int)SysTime.wDay;
}

//date.1970s
int DATE::CMD4::run(const vector<string> &args)
{
    return (int)time(NULL);
}

//date.format
int DATE::CMD5::run(const vector<string> &args)
{
    char szBuf[256];
    string::size_type Pos;
    SYSTEMTIME SysTime;
    
    CHECK_UTC_PARA(1, args);

    if (args.size() == 1)
    {
        GetLocalTime(&SysTime);
    }
    else
    {
        GetSystemTime(&SysTime);
    }

    string DateStr = args[0];

    DATE_STR_REPLACE("%Y", "%04u", SysTime.wYear);
    DATE_STR_REPLACE("%m", "%02u", SysTime.wMonth);
    DATE_STR_REPLACE("%d", "%02u", SysTime.wDay);
    
    DATE_STR_REPLACE("%H", "%02u", SysTime.wHour);
    DATE_STR_REPLACE("%M", "%02u", SysTime.wMinute);
    DATE_STR_REPLACE("%S", "%02u", SysTime.wSecond);




    
    print("%s\n", DateStr.c_str());
    
    return ERR_SUCCESS;
}

